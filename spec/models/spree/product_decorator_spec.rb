require 'spec_helper'

describe "Product scopes and class method", type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "scopes" do
    let(:product_in_same_taxon) { create(:product, taxons: [taxon]) }

    it "in_same_taxon" do
      expect(Spree::Product.in_same_taxon(product)).to eq [product, product_in_same_taxon]
    end

    it "exclude_product" do
      expect(Spree::Product.exclude_product(product)).to eq [product_in_same_taxon]
    end
  end

  describe "class methods" do
    let(:rails) { create(:taxon) }
    let(:rails_products) { create_list(:product, 3, taxons: [rails]) }

    before do
      product.taxons << rails
    end

    it "related_products" do
      expect(Spree::Product.related_products_to(product)).to eq rails_products
    end
  end
end
