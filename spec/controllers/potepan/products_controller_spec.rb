require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "responds successfully" do
      expect(response).to be_success
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "display show.html" do
      expect(response).to render_template :show
    end

    it "returns @product" do
      expect(assigns(:product)).to eq product
    end

    describe "related_products" do
      let(:bag) { create(:taxon) }
      let(:bag_product) { create(:product, taxons: [bag]) }

      context "when 4 related_products" do
        let(:shirt) { create(:taxon) }
        let(:other_4_bag_products) { create_list(:product, 4, taxons: [bag]) }
        let(:shirt_product) { create(:product, taxons: [shirt]) }

        before do
          get :show, params: { id: bag_product.id }
        end

        it "returns @related_products" do
          expect(assigns(:related_products)).to eq other_4_bag_products
        end

        it "is not include shirt_product" do
          expect(assigns(:related_products)).not_to include shirt_product
        end
      end

      context "when more than 4 related_products" do
        let!(:other_5_bag_products) { create_list(:product, 5, taxons: [bag]) }

        before do
          get :show, params: { id: bag_product.id }
        end

        it "includes only 4 products" do
          expect(assigns(:related_products).count).to eq(4)
        end
      end
    end
  end
end
