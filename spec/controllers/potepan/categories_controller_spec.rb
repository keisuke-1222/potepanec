require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy_brands) { create(:taxonomy, name: "Brand") }
    let!(:taxonomy_categories) { create(:taxonomy, name: "Categories") }

    let(:taxon_parent) { taxonomy_brands.root }
    let(:taxon_child) { create(:taxon, parent: taxon_parent) }

    let!(:product_parent) { create(:product, taxons: [taxon_parent]) }
    let!(:product_child) { create(:product, taxons: [taxon_child]) }

    before do
      get :show, params: { id: taxon_parent.id }
    end

    it "responds successfully" do
      expect(response).to be_success
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "display show.html" do
      expect(response).to render_template :show
    end

    it "returns @taxon" do
      expect(assigns(:taxon)).to eq taxon_parent
    end

    it "returns @pruducts" do
      expect(assigns(:products)).to match [product_parent, product_child]
    end

    describe "@brands and @categories" do
      let(:brands) { taxonomy_brands.root }
      let(:categories) { taxonomy_categories.root }

      let!(:brands_list) { create_list(:taxon, 3, parent: brands) }
      let!(:categories_list) { create_list(:taxon, 3, parent: categories) }

      before do
        get :show, params: { id: brands.id }
      end

      it "returns @brands" do
        expect(assigns(:brands)).to match [taxon_child, *brands_list]
      end

      it "returns @categories" do
        expect(assigns(:categories)).to match categories_list
      end
    end
  end
end
