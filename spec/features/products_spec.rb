require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:taxonomy_brands) { create(:taxonomy, name: "Brand") }
  given(:taxonomy_categories) { create(:taxonomy, name: "Categories") }

  given(:brands) { taxonomy_brands.root }
  given(:nike) { create(:taxon, parent: brands) }

  given(:categories) { taxonomy_categories.root }
  given(:bags) { create(:taxon, parent: categories) }

  given(:main_product) { create(:product, taxons: [nike, bags]) }
  given!(:nike_product) { create(:product, taxons: [nike]) }
  given!(:bags_product) { create(:product, taxons: [bags]) }

  scenario "user visit a single product page and related product page" do
    visit potepan_product_path(main_product.id)

    expect(page).to have_content main_product.name
    expect(page).to have_content main_product.description
    expect(page).to have_content main_product.display_price

    # 関連商品名が表示されている
    expect(page).to have_content nike_product.name
    expect(page).to have_content bags_product.name

    click_link nike_product.name

    expect(page).to have_content nike_product.name
    expect(page).to have_content nike_product.description
    expect(page).to have_content nike_product.display_price

    # 無関連の商品名が表示されていない
    expect(page).not_to have_content bags_product.name

    click_link '一覧ページへ戻る'

    expect(page.current_path).to eq potepan_category_path(nike_product.taxons.first.id)

    # nike_productのみが表示されている
    expect(page).to have_content nike.name
    expect(page).to have_content nike_product.name
    expect(page).not_to have_content bags_product.name
  end
end
