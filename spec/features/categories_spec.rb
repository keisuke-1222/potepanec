require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given(:taxonomy_brands) { create(:taxonomy, name: "Brand") }
  given(:taxonomy_categories) { create(:taxonomy, name: "Categories") }

  given(:brands) { taxonomy_brands.root }
  given(:rails) do
    create(:taxon, {
      parent: brands,
      name: "Rails",
    })
  end

  given(:categories) { taxonomy_categories.root }
  given(:bags) do
    create(:taxon, {
      parent: categories,
      name: "Bags",
    })
  end

  given!(:rails_product) { create(:product, taxons: [rails]) }
  given!(:bags_product) { create(:product, taxons: [bags]) }

  scenario "user visit brand page and categories page" do
    visit potepan_category_path(rails.id)

    expect(page).to have_content rails.name
    expect(page).to have_content rails_product.name

    click_link "Bags"

    expect(page).to have_content bags.name
    expect(page).to have_content bags_product.name

    click_link bags_product.name

    expect(page).to have_content bags_product.name
    expect(page).to have_content bags_product.description
    expect(page).to have_content bags_product.display_price
  end
end
