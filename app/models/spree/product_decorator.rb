Spree::Product.class_eval do
  def self.related_products_to(product)
    includes_price_and_image.
      in_same_taxon(product).
      exclude_product(product).
      distinct
  end

  scope :includes_price_and_image, -> { includes(master: [:default_price, :images]) }
  scope :in_same_taxon, ->(product) {
    joins(:taxons).
      where(spree_taxons: { id: product.taxons.ids })
  }
  scope :exclude_product, ->(product) { where.not(id: product.id) }
end
