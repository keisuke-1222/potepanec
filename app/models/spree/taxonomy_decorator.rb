Spree::Taxonomy.class_eval do
  scope :leaves_from_root_name, ->(name) { find_by!(name: name).root.leaves }
end
