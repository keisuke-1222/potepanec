module ApplicationHelper
  def full_title(title = '')
    base_title = "BIGBAG Store"
    title.empty? ? base_title : "#{title} | #{base_title}"
  end
end
