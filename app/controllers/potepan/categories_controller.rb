class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.includes_price_and_image.in_taxon(@taxon)
    @brands = Spree::Taxonomy.leaves_from_root_name("Brand")
    @categories = Spree::Taxonomy.leaves_from_root_name("Categories")
    @display = params[:display] == "list" ? "list" : "grid"
  end
end
