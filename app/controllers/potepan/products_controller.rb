class Potepan::ProductsController < ApplicationController
  MAX_RELATED_PRODUCTS_NUM = 4

  def show
    @product = Spree::Product.find(params[:id])
    @images = @product.images
    @related_products = Spree::Product.
      related_products_to(@product).
      limit(MAX_RELATED_PRODUCTS_NUM)
  end
end
